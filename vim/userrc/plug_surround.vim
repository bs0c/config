let g:surround_no_mappings=1
nmap ds  <Plug>Dsurround
nmap cs  <Plug>Csurround
nmap cS  <Plug>CSurround
nmap ls  <Plug>Ysurround
nmap lS  <Plug>YSurround
nmap lss <Plug>Yssurround
nmap lSs <Plug>YSsurround
nmap lSS <Plug>YSsurround
xmap S   <Plug>VSurround
xmap gS  <Plug>VgSurround
