let g:EasyMotion_keys = 'arstdhneio'
let g:EasyMotion_do_shade = 0
map ' <Plug>(easymotion-prefix)
autocmd FileType easymotion map e
autocmd FileType easymotion map k
nmap <Leader>n <Plug>(easymotion-j)
nmap <Leader>e <Plug>(easymotion-k)
