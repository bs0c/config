"###   global settings   ###

" This is colemak bind key for vim
" to swap y <=> l Y <=> L n <=> j k <=> e

" Up/down/left/right

nnoremap n gj|xnoremap n gj|onoremap n gj|
nnoremap e gk|xnoremap e gk|onoremap e gk|
nnoremap y l|xnoremap y l|onoremap y l|

"nnoremap e k|xnoremap e k|onoremap e k|
"nnoremap n j|xnoremap n j|onoremap n j|

" Words forward/backward 

nnoremap k e|xnoremap k e|onoremap k e|
nnoremap K E|xnoremap K E|onoremap K E|

" Cut/copy/paste

nnoremap l y|xnoremap l y|
nnoremap ll yy|xnoremap ll yy|
nnoremap L Y|xnoremap L Y|

" Search

nnoremap j n|xnoremap j n|
nnoremap J N|xnoremap J N|

" Склейка строк

nnoremap N J|xnoremap N J|

" Folding

nnoremap zn zj|xnoremap zn zj|onoremap zn zj|
nnoremap ze zk|xnoremap ze zk|onoremap ze zk|

nnoremap zJ zN|xnoremap zJ zN|onoremap zJ zN|
nnoremap zj zn|xnoremap zj zn|onoremap zj zn|

" Window handling

nnoremap <C-W>h <C-W>h|xnoremap <C-W>h <C-W>h|
nnoremap <C-W>n <C-W>j|xnoremap <C-W>n <C-W>j|
nnoremap <C-W>N <C-W>J|xnoremap <C-W>N <C-W>J|
nnoremap <C-W>e <C-W>k|xnoremap <C-W>e <C-W>k|
nnoremap <C-W>E <C-W>K|xnoremap <C-W>E <C-W>K|
nnoremap <C-W>y <C-W>l|xnoremap <C-W>y <C-W>l|
nnoremap <C-W>Y <C-W>L|xnoremap <C-W>Y <C-W>L|

"Create new split

nnoremap <C-W>j <C-W>n|xnoremap <C-W>j <C-W>n|
