"###   global settings   ###

map <Down> <Nop>
map <Up> <Nop>
map <Left> <Nop>
map <Right> <Nop>

map <Home> <Nop>
map <End> <Nop>
map <PageUp> <Nop>
map <PageDown> <Nop>

map <LeftMouse> <Nop>
map <MiddleMouse> <Nop>
map <RightMouse> <Nop>
map <ScrollWheelUp> <Nop>
map <ScrollWheelDown> <Nop>
map <ScrollWheelLeft> <Nop>
map <ScrollWheelRight> <Nop>
