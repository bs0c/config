"###   user settings   ###

call plug#begin()

    Plug 'tpope/vim-surround'
        source $XDG_CONFIG_HOME/vim/userrc/plug_surround.vim
    Plug 'Lokaltog/vim-easymotion'
        source $XDG_CONFIG_HOME/vim/userrc/plug_easymotion.vim
    Plug 'vimwiki/vimwiki'
        source $XDG_CONFIG_HOME/vim/userrc/plug_vimwiki.vim
    Plug 'junegunn/vim-easy-align'
        source $XDG_CONFIG_HOME/vim/userrc/plug_vim-easy-align.vim
    Plug 'tpope/vim-commentary'
    Plug 'mhinz/vim-rfc'

"###   syntax   ###
    Plug 'nfnty/vim-nftables'

"###   themes   ###

    Plug 'jonathanfilip/vim-lucius'
        source $XDG_CONFIG_HOME/vim/userrc/plug_lucius.vim
    Plug 'sonph/onehalf', { 'rtp': 'vim' }
    Plug 'NLKNguyen/papercolor-theme'
    Plug 'rakr/vim-one'
    Plug 'mhartington/oceanic-next'
    Plug 'ayu-theme/ayu-vim'

call plug#end()

set cursorline
color lucius

