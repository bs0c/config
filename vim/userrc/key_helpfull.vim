"###   global settings   ###

nmap <F6> :set paste! <CR>
imap <F6> <ESC> :set paste! <CR>i

"###   encoding file   ###

" change encoding
menu Encoding.Read.UTF8<TAB><F7> :e ++enc=utf8 <CR>
menu Encoding.Read.KOI8-R<TAB><F7> :e ++enc=koi8-r<CR>
menu Encoding.Read.CP1251<TAB><F7> :e ++enc=cp1251<CR>
menu Encoding.Read.CP866<TAB><F7> :e ++enc=cp866<CR>
map <F7> :emenu Encoding.Read.<TAB>

" write encoding
menu Encoding.Write.UTF8<TAB><F8> :set fenc=utf8 <CR>
menu Encoding.Write.KOI8-R<TAB><F8> :set fenc=koi8-r<CR>
menu Encoding.Write.CP1251<TAB><F8> :set fenc=cp1251<CR>
menu Encoding.Write.CP866<TAB><F8> :set fenc=cp866<CR>
map <F8> :emenu Encoding.Write.

"###   spell   ###

" check spell
menu SpellLang.nospell<TAB><F9> :setlocal nospell <CR>
menu SpellLang.RU<TAB><F9> :setlocal spell spelllang=ru_yo <CR>
menu SpellLang.EN<TAB><F9> :setlocal spell spelllang=en_us <CR>
menu SpellLang.RU_EN<TAB><F9> :setlocal spell spelllang=ru_ru,en_us <CR>
map <F9> :emenu SpellLang.<TAB>

nmap <F10> :set nu! <CR>
imap <F10> <ESC> :set nu! <CR>i

nmap <F11> :set rnu! <CR>
imap <F11> <ESC> :set rnu! <CR>i

nmap <leader>h :set hls! <CR>
nmap <leader>r :source $MYVIMRC <CR>

" Tab

"nmap <Tab> :tabnext<CR>
nmap <S-Tab> :tabprevious<CR>
nmap <C-t>n :tabnew<CR>
nmap <C-t>N :tabnew 
nmap <C-t>x :tabclose<CR>
nmap <C-t>X :tabonly<CR>

