#!/usr/bin/env bash
# global settings

HISTSIZE=10000
HISTTIMEFORMAT="%h %d %H:%M:%S "
HISTCONTROL="ignoreboth:erasedups"
HISTIGNORE="&:l[ls]:[bf]g:exit:pwd:clear:mount:umount"
PROMPT_COMMAND="history -a; history -n"

shopt -s cmdhist
