#!/usr/bin/env bash
# global settings

ash ()
{
    [[ -n $1 ]] || {
        echo "$FUNCNAME: needs an argument"
        return 1
    }
    alias | grep $1 || echo "$FUNCNAME: don't find any aliase for \"$1\""
}

load_alias_system ()
{
    ###   ls   ###
    alias l.="ls -d .* --color=auto"
    alias ll="ls -lah --color=auto"
    alias lls="ls -lah --color=auto --sort=size"
    alias llt="ls -lah --color=auto --sort=time"

    ###   du   ###
    alias du="du -h"

    ###   df   ###
    alias df="df -h"

    ###   lsblk   ###
    alias lsb="lsblk"

    ###   ip   ###
    alias ip="ip --color=auto"

    ###   nft   ###
    alias nftl="nft list ruleset"
    alias nfth="nft --handle list ruleset"
    alias nftr="nft flush ruleset"

    ###   journalctl   ###
    alias jb="journalctl --list-boots --no-pager"
    alias jbn="journalctl --pager-end --boot"
    alias jl="journalctl  --pager-end --catalog --no-pager"
    alias jlf="journalctl --pager-end --catalog --follow"
    alias jlu="journalctl --pager-end --catalog  --no-pager -u"
    alias jul="journalctl  --user --pager-end --catalog"
    alias julf="journalctl --user --pager-end --catalog --follow"
    alias julu="journalctl --user --pager-end --catalog  --no-pager -u"

    ###   systemd   ###
    alias sst="systemctl status "
    alias sdr="systemctl daemon-reload"
    alias sls="systemctl show -p After,Before,Requires,Wants,WantedBy,RequiredBy "
    alias sust="systemctl --user status"
    alias sudr="systemctl --user daemon-reload "
    alias suls="systemctl --user show -p After,Before,Requires,Wants,WantedBy,RequiredBy "

    ###   mount   ###
    alias mntt="mount | column -t"

    ###   calendar   ###
    alias cal="cal -m"
    alias cal3="cal -m -3"
    alias caly="cal -m -y"
    alias calY="cal -m -Y"

    ###   tmux   ###
    alias tma="tmux attach -t"
    alias tml="tmux list-session"

    alias edsa="vi $XDG_CONFIG_HOME/bash/bashrc.d/80-alias_system.sh"
}

load_alias_pkg ()
{
    if [[ $(id -u) -eq 0 ]]; then
        local os=$(grep -oP '(?<=^ID=).+' /etc/os-release)
        case $os in
            gentoo)
                alias pupd="EMERGE_DEFAULT_OPTS='' eix-sync"
                alias pupg="EMERGE_DEFAULT_OPTS='' emerge --ask --verbose --update --deep --newuse world"
                alias pcln="emerge --depclean"
                ;;
            debian)
                alias pupd="apt update"
                alias pupg="apt upgrade"
                alias pcln="apt autoremove"
                ;;
            rhel|centos)
                alias pupd="dnf check-update"
                alias pupg="dnf upgrade"
                alias pcln="dnf autoremove"
                ;;
            *)
                echo "There isn't write about: \"$os\""
                return 1
                ;;
        esac
    fi
}

load_alias_system
load_alias_pkg
