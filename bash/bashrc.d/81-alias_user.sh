#!/usr/bin/env bash

__get_translation__()
{
    [[ -f ${2-} ]] || {
        echo "Usage: aetr <filename>"
        return 1
    }
    echo -e "translating...\c"
    cat "$2" | eval ARGOS_DEVICE_TYPE=cuda argos-translate "$1" > "${2}"_ru && \
    echo -e "\tget the result in ${2}_ru"
}

__trans_and_play_trans_world__ ()
{
    trans $1:$2 $3; trans $2:$1 -speak -no-translate $(trans $1:$2 $3 -b)
}

__pinyin () {
    [[ -n ${1-} ]] || {
        echo "Usage: pinyin <chinese_word>"
        return 1
    }
    trans zh-CN:en "$1" | sed  -rn "2s/[(')]//g;2s/(.*)/\L\1/p" | xclip -i -selection clipboard
    xclip -o -selection clipboard
}

__to_mono () {
    [[ -f ${1-} ]] || {
        echo "Usage: to_mono <path-to-file>"
        return 1
    }
    local name=${1%%.*}
    local ext=${1##*.}
    ffmpeg -loglevel quiet -i $1 -ac 1 ${name}_mono.${ext} && \
        echo "done: ${name}_mono.${ext}"
    ffmpeg -loglevel quiet -i $1 -filter_complex "[0:a]channelsplit=channel_layout=stereo[left][right]" -map "[left]" ${name}_left.${ext} -map "[right]" ${name}_right.${ext} && \
        echo "done: ${name}_left.${ext} ${name}_right.${ext}"
}

__get_subtitle__()
{
    echo "make str from $1"
    ffmpeg -loglevel quiet -i ${1-} -map 0:s:0 "${1%.*}".srt && \
        echo "${1%.*}.srt: created"
}

__get_sound__()
{
    echo "make mp3 from $1"
    ffmpeg -loglevel quiet -i "$1" -codec:a libmp3lame -qscale:a 2 "${1%.*}".mp3 && \
        echo "${1%.*}.mp3: created"
}

__do_with_list__()
{
    action=$1; shift
    for obj in "$@"; do
        [[ -f $obj ]] || {
            echo "file: \"$1\" not found, skip..."
            continue
        }
        $action "$obj"
    done
}

load_alias_user ()
{
    alias edua="vi $XDG_CONFIG_HOME/bash/bashrc.d/81-alias_user.sh"

    ###   edit config   ###

    alias edtm="vi $XDG_CONFIG_HOME/tmux/tmux.conf"
    alias edti="vi $XDG_CONFIG_HOME/tmux/init.sh"
    alias edaw="vi $XDG_CONFIG_HOME/awesome/rc.lua"
    alias edvi="vi $XDG_CONFIG_HOME/vim/vimrc"
    alias edbr="vi $XDG_CONFIG_HOME/bash/bashrc"
    alias edbp="vi $XDG_CONFIG_HOME/bash/bash_profile"

    ###   power   ###

    alias suspend="sudo s2ram -f -a 1"

    ###   mount   ###

    alias mnt="udevil mount"
    alias umnt="udevil umount"
    alias mtp='go-mtpfs ${HOME}/phone &'
    alias umtp='cd ${HOME} ; fusermount -u ${HOME}/phone'

    ###   translate   ###

    alias aetr="__get_translation__ '--from-lang en --to-lang ru'"
    alias arte="__get_translation__ '--from-lang ru --to-lang en'"
    alias etr="trans en:ru -speak"
    alias etc="__trans_and_play_trans_world__ en zh-CN "
    alias rte="trans ru:en"
    alias rtc="__trans_and_play_trans_world__ ru zh-CN "
    alias cte="trans zh-CN:en -speak"
    alias ctr="trans zh-CN:ru -speak"
    alias pinyin="__pinyin "
    alias to-mono="__to_mono "

    ###   stream video music  ###

    local proxy='--proxy socks5://127.0.0.1:1081'
    alias yt="yt-dlp $proxy"
    alias ytl="yt-dlp --format-sort 'res:720' $proxy"
    alias yta="yt-dlp --extract-audio --audio-quality 0 $proxy"

    alias mls="mpv --vo=null --ytdl-raw-options=proxy=http://127.0.0.1:1081"

    alias get_srt="__do_with_list__ __get_subtitle__ "
    alias get_snd="__do_with_list__ __get_sound__ "

    ###   timer   ###
    alias t-eyes="timer --sound --notify eyes & disown"
    alias t-back="timer --sound --notify back & disown"
    alias t-study="timer --sound --notify study & disown"
    alias t-hands="timer --sound --notify hands & disown"
    alias t-language="timer --sound --notify language & disown"

    ###   flatpak   ###
    alias anki="nohup flatpak --user run net.ankiweb.Anki &>/dev/null & disown"
    alias whitenoise="nohup flatpak --user run com.rafaelmardojai.Blanket & disown"
}

load_alias_user
