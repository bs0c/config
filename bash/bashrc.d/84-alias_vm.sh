#!/usr/bin/env bash

__virtual_stand ()
{
    local list_vm=
    local ip=
    case ${2-} in
        f | firewall) list_vm="srv cli1 cli2";;
        d | default) list_vm="d-alma d-debian d-rhel d-fbsd";;
        s | sql) list_vm="sql-postgres sql-mysql";;
        *)  echo "The wrong virtual stand: ${2-}"
            echo "usage: vss [f d s]"
            return 1
            ;;
    esac
    for vm in $list_vm; do
        virsh $1 $vm
    done
    case $1 in
        start)
            sshi || ssha
            [[ ${2-} =~ ^d ]] && return 0               # don't create tmux session for default
            echo "wait to start vm..."
            until ssh ${list_vm##* }-u exit &>/dev/null; do
                sleep 10
            done
            [[ ${2-} =~ d(efault)? ]] || ts create virt -w $list_vm
            ;;
        destroy)
            [[ ${2-} =~ d(efault)? ]] || ts kill virt
            ;;
        *)
            echo "Wrong action to virtual stand: ${1-}"
            return 1
            ;;
    esac
    return 0
}

load_alias_vm ()
{
    alias edvm="vi $XDG_CONFIG_HOME/bash/bashrc.d/84-alias_vm.sh"
    alias vss="__virtual_stand start "
    alias vsd="__virtual_stand destroy "
    alias vsr="__virtual_stand reboot "
    alias vsst="virsh list"
    alias vsw="virsh start win10"
    alias vdw="virsh destroy win10"
    alias vw="GDK_SCALE=2 nohup virt-viewer --hotkeys=toggle-fullscreen=shift+f12,release-cursor=ctrl+alt &>/dev/null & disown"
    alias vww="GDK_SCALE=2 nohup virt-viewer --full-screen --attach win10 --hotkeys=toggle-fullscreen=shift+f12,release-cursor=ctrl+alt &>/dev/null & disown"
}

load_alias_vm
