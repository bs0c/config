#!/usr/bin/env bash
# global settings

shopt -s cdspell
shopt -s direxpand
shopt -s checkjobs

###   editor   ###
export EDITOR=/usr/bin/vi
export SYSTEMD_EDITOR=/usr/bin/vi
