#!/usr/bin/env bash
# global settings

function color_ps
{
    #--------------------------------------------------------------------------------------------+
    #            Color picker, usage: printf ${BLD}${CUR}${RED}${BBLU}"Hello!)"${DEF}            |
    #-------------------------------------+--------------------------------------------+---------+
    #             Text color              |               Background color             |         |
    #-----------------+-------------------+--------------------+-----------------------+         |
    #    Base color   |   Lighter shade   |     Base color     |     Lighter shade     |         |
    #-----------------+-------------------+--------------------+-----------------------+         |
    local BLK='\e[30m'; local blk='\e[90m'; local BBLK='\e[40m'; local bblk='\e[100m' #| Black   |
    local RED='\e[31m'; local red='\e[91m'; local BRED='\e[41m'; local bred='\e[101m' #| Red     |
    local GRN='\e[32m'; local grn='\e[92m'; local BGRN='\e[42m'; local bgrn='\e[102m' #| Green   |
    local YLW='\e[33m'; local ylw='\e[93m'; local BYLW='\e[43m'; local bylw='\e[103m' #| Yellow  |
    local BLU='\e[34m'; local blu='\e[94m'; local BBLU='\e[44m'; local bblu='\e[104m' #| Blue    |
    local MGN='\e[35m'; local mgn='\e[95m'; local BMGN='\e[45m'; local bmgn='\e[105m' #| Magenta |
    local CYN='\e[36m'; local cyn='\e[96m'; local BCYN='\e[46m'; local bcyn='\e[106m' #| Cyan    |
    local WHT='\e[37m'; local wht='\e[97m'; local BWHT='\e[47m'; local bwht='\e[107m' #| White   |
    #--------------------------------------------------------------------------------------------+
    # Effects                                                                                    |
    #--------------------------------------------------------------------------------------------+
    local DEF='\e[0m'   #Default color and effects                                               |
    local BLD='\e[1m'   #Bold\brighter                                                           |
    local DIM='\e[2m'   #Dim\darker                                                              |
    local CUR='\e[3m'   #Italic font                                                             |
    local UND='\e[4m'   #Underline                                                               |
    local INV='\e[7m'   #Inverted                                                                |
    local COF='\e[?25l' #Cursor Off                                                              |
    local CON='\e[?25h' #Cursor On                                                               |
    #--------------------------------------------------------------------------------------------+

    export TERM=xterm-256color

    local b='\['
    local e='\]'

    local start=${b}$BLD${e}
    local end=${b}$DEF${e}

    local brk=${b}$BLK${e}
    local path=${b}$YLW${e}
    local result=$path

    local host=$brk
    [[ $HOSTNAME =~ ^wst ]] && host=${b}$BLU${e}
    [[ $HOSTNAME =~ ^srv ]] && host=${b}$MGN${e}
    [[ $HOSTNAME =~ ^phone ]] && host=${b}$CYN${e}
    [[ $HOSTNAME =~ ^[vc] ]] && host=${b}$YLW${e}

    local user=${b}$GRN${e}
    [[ $EUID == 0 ]] && user=${b}$RED${e}

    PS1=$start$brk'┌─['$result\$?$brk']─['$user'\u'$brk'@'$host'\H'$brk']─['$path'\w'$brk']\n└─'$user'\$ '${end}
}

color_ps
