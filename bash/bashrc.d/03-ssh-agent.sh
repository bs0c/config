#!/usr/bin/env bash

__ssh_agent__()
{
    [[ -n $SSH_AUTH_SOCK ]] || {
        echo "env \$SSH_AUTH_SOCK is empty."
        return 1
    }
    local cli="ssh-agent"
    local list_keys="vm,vm-r,srv,gitlab"

    case $1 in
        start)
            __ssh_agent__ stop
            $cli -a $SSH_AUTH_SOCK > /dev/null
            eval ssh-add $HOME/.ssh/{$list_keys}
            ;;
        stop)
            ps -C $cli &>/dev/null && pkill --signal SIGKILL $cli
            [[ -e $SSH_AUTH_SOCK ]] && rm --force $SSH_AUTH_SOCK
            ;;
        info)
            local pid=$(ps -C $cli -o pid=)
            if [[ -n $pid ]]; then
                echo "ssh-agent has been run and has a pid:$pid"
                return 0
            else
                echo "ssh-agent wasn't run"
                return 1
            fi
            ;;
        *) echo "Something went wrong!";;
    esac
}

alias ssha="__ssh_agent__ start"
alias sshs="__ssh_agent__ stop"
alias sshi="__ssh_agent__ info"
