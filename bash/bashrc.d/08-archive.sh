#!/usr/bin/env bash

# Usage: pack [format archive] <list_files>
# Usage: unp  <file>
# Usage: unpl <list_file>

__package ()
{
    local file_count=0
    for item in "$@"; do
        [[ ! -f $item ]] || (( file_count++ ))
    done

    if (( file_count == 1 )); then
        local archive="${!#}"
    else
        local archive=${PWD##*/}
    fi

    case $1 in
        tbz2|bz2)   archive+=".tar.$1"
                    shift
                    tar -cjSf "$archive" "$@"
                    ;;
        tgz|gz)     archive+=".tar.$1"
                    shift
                    tar -czSf "$archive" "$@"
                    ;;
        xz)         archive+=".tar.$1"
                    shift
                    tar -cJSf "$archive" "$@"
                    ;;
        7z)         shift
                    7z a "$archive" "$@" 1>/dev/null
                    ;;
        zip)        shift
                    zip -q "$archive" "$@"
                    ;;
        *)          archive+=".tar.zst"
                    tar -I 'zstd -3' -cf "$archive" "$@"
                    ;;
    esac
    [[ $? ]] || return 1
    echo "===> $archive"
}

__unpack()
{
    local archive=$(readlink -f "${1-}")
    local is_single=$(basename "${1-}")
    local dest=$(echo $is_single | cut -d '.' -f 1)

    [[ -f $archive ]] || {
        echo "can't find archive: \"$archive\""
        return 1
    }

    local base="$dest"
    local n=2
    while [[ -d $dest ]] || [[ -f $dest ]]; do
        dest="$base-$n"
        (( n++ ))
    done
    dest="$PWD/$dest"
    mkdir "$dest"

    case "$archive" in
# ----- tar
        *.tar)            tar   -xvf        "$archive" -C "$dest";;
        *.tbz2|*.tar.bz2) tar   -xjvf       "$archive" -C "$dest";;
        *.tgz|*.tar.gz)   tar   -xzvf       "$archive" -C "$dest";;
        *.tzst|*.tar.zst) tar   --zstd -xvf "$archive" -C "$dest";;
# ----- other
        *.7z)             7za   x           "$archive" -o "$dest";;
        *.rar)            unrar x           "$archive"    "$dest";;
        *.zip|*.apk)      unzip             "$archive" -d "$dest";;
# ----- single
        *.xz)             unxz              "$archive" -c>"$dest/${is_single%.*}";;
        *.gz)             gunzip            "$archive" >  "$dest/${is_single%.*}";;
        *.bzip2|*.bz2)    bunzip2           "$archive" >  "$dest/${is_single%.*}";;
        *.zst)            unzstd            "$archive" -o "$dest/${is_single%.*}";;
        *)  echo "doesn't support format: \"${archive##*.}\""
            rmdir $tmp
            return 1
            ;;
    esac

    [[ $? ]] && echo "===> \"$dest/\""

    return 0
}

__unpack_list ()
{
    for archive in $@; do
        unpack "$archive" || exit 1
    done
}

alias pack="__package "
alias unp="__unpack "
alias unpl="__unpack_list "
