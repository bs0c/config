#!/usr/bin/env bash
# global setting

_cert_show() {
    [[ $# -eq 1 ]] || {
        echo "usage: crtsh <url | file>"
        return 1
    }
    if [[ -f $1 ]]; then
        openssl x509 -text -noout -in $1
    else
        local port=${2:-443}
        local trustCA=${3:-} # -CAfile
        echo | openssl s_client -showcerts -connect $1:$port
    fi
    return 0
}

_cert_get() {
    [[ $# -eq 1 ]] || {
        echo "usage: crtsh <url>"
        return 1
    }
    local port=${2:-443}
    openssl s_client -showcerts -connect $1:$port </dev/null 2>/dev/null | openssl x509 -outform PEM >$1-$port.pem && \
        echo "cert saved in $(pwd)/$1-$port.pem"
}

load_alias_cert ()
{
    alias crtsh="_cert_show "
    alias crtgt="_cert_get "
}

load_alias_cert
