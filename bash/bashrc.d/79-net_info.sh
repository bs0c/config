#!/usr/bin/env bash
# global settings

net_info()
{
    [[ $# == 1 ]] || {
        echo "usage: $FUNCNAME ip/mask"
        return 1
    }
    type bc 1>/dev/null || return 1

    __str_to_num()
    {
        local r= i=3 IFS='.'
        for oct in $1; do
            let "tmp = oct << (i * 8)" "r += tmp" "i--"
        done
        echo $r
    }
    __num_to_str()
    {
        local r="" mask=255 bit=$1
        for _ in {1..4}; do
            r=$((bit & mask)).$r
            ((bit >>= 8))
        done
        echo ${r::-1}
    }
    __mask_to_num()
    {
        local r=$(eval printf -- '1%.0s' {1..$1})
        [[ $1 -ge 32 ]] || {
            r=$r$(eval printf -- '0%.0s' {1..$((32-$1))})
        }
        echo "ibase=2; $r" | bc
    }
    __set_mask()
    {
        if [[ $mask_str =~ ^([0-9]{1,3}\.){3}[0-9]{1,3}$ ]]; then
            mask_num=$(__str_to_num $mask_str)
            local cnt=0 num=$mask_num
            while [[ $num -gt 0 ]]; do
                ((cnt += num & 1, num >>= 1))
            done
            mask_str="$cnt ($mask_str)"
        elif [[ $mask_str =~ ^([1-9]|[1,2][0-9]|3[0-2])$ ]]; then
            mask_num=$(__mask_to_num $mask_str)
            mask_str="$mask_str ($(__num_to_str $mask_num))"
        else
            printf "mask: \"%s\" is wrong\n" $mask_str
            return 1
        fi
        return 0
    }
    local ip_str=${1%/*} ip_num=$(__str_to_num ${1%/*})
    local mask_str=${1#*/} mask_num=0
    if __set_mask ; then
        cat <<EOF

    id:    $(__num_to_str $((ip_num & (4294967295 - mask_num))))
    hosts: $((4294967295 - mask_num))
    net:   $(__num_to_str $((ip_num & mask_num)))/$mask_str
    brd:   $(__num_to_str $(((ip_num & mask_num) + (4294967295 - mask_num) )))

EOF
    fi
}
