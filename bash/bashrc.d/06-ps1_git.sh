#!/usr/bin/env bash

function load_bash_git_prompt
{
    local run_file="$THIRD_PARTY_LIBS/bash-git-prompt/gitprompt.sh"
    local repo="https://github.com/magicmonty/bash-git-prompt.git"

    [[ -e $run_file ]] || git clone --quiet $repo "${run_file%/*}"

    if [[ -e $run_file ]]; then
        GIT_PROMPT_ONLY_IN_REPO=1
            # uncomment to avoid fetching remote status
        GIT_PROMPT_FETCH_REMOTE_STATUS=0
            # uncomment to avoid searching for changed files in submodules
        # GIT_PROMPT_IGNORE_SUBMODULES=1
            # uncomment to avoid setting virtual environment infos for node/python/conda environments
        # GIT_PROMPT_WITH_VIRTUAL_ENV=0
            # uncomment to show upstream tracking branch
        # GIT_PROMPT_SHOW_UPSTREAM=1
            # can be no, normal or all; determines counting of untracked files
        # GIT_PROMPT_SHOW_UNTRACKED_FILES=normal
            # uncomment to avoid printing the number of changed files
        # GIT_PROMPT_SHOW_CHANGED_FILES_COUNT=0
            # uncomment to support Git older than 1.7.10
        GIT_PROMPT_STATUS_COMMAND=gitstatus_pre-1.7.10.sh
            # uncomment for custom prompt start sequence
        GIT_PROMPT_START=$(echo $PS1 | sed -e 's/\\n.*$//')
        GIT_PROMPT_END='\n'$(echo $PS1 | sed -e 's/.*\\n//')
            # as last entry source the gitprompt script
            # use custom theme specified in file GIT_PROMPT_THEME_FILE (default ~/.git-prompt-colors.sh)
        GIT_PROMPT_THEME_FILE="$bashrc_d/07-ps1_git.theme"
            # use theme optimized for solarized color scheme
        # GIT_PROMPT_THEME=Solarised
        source "$run_file"
    fi
}

load_bash_git_prompt
