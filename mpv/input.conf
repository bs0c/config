# Numpad

KP0      ignore
KP1      ignore
KP2      ignore
KP3      ignore
KP4      ignore
KP5      ignore
KP6      ignore
KP7      ignore
KP8      ignore
KP9      ignore
KP_DEC   ignore
KP_ENTER ignore

# Mouse

MOUSE_BTN0     ignore
MOUSE_BTN0_DBL ignore
MOUSE_BTN2     ignore
MOUSE_BTN3     ignore
MOUSE_BTN4     ignore
MOUSE_BTN5     ignore
MOUSE_BTN6     ignore

# Arrow/navigation keys

RIGHT ignore
LEFT  ignore
UP    ignore
DOWN  ignore
PGUP  ignore
PGDWN ignore

# ` [1] [2] [3] [4] [5] [6] [7] [8] [9] [0]
# ~ [!] [@]  #   $   %   ^   &   *   (   )

1      add contrast -1 ; show-text "Contrast: ${contrast}"
2      add contrast +1 ; show-text "Contrast: ${contrast}"
3      add brightness -1 ; show-text "Brightness: ${brightness}"
4      add brightness +1 ; show-text "Brightness: ${brightness}"
5      add gamma -1 ; show-text "Gamma: ${gamma}"
6      add gamma +1 ; show-text "Gamma: ${gamma}"
7      add saturation -1 ; show-text "Saturation: ${saturation}"
8      add saturation +1 ; show-text "Saturation: ${saturation}"
9      add volume -2 ; show-text "Volume: ${volume}"
0      add volume +2 ; show-text "Volume: ${volume}"

!      cycle ontop
@      cycle-values vf "format=colorlevels=full" "format=colorlevels=auto" "format=colorlevels=limited"

#      ignore
$      ignore
%      ignore
^      ignore
&      ignore
*      ignore

# [q] [w] [f] [p] g j [l] u [y] ; [ ]
# [Q] [W] [F]  P  G J [L] U [Y] : { }

q      quit-watch-later
Q      quit

w      script-message osc-playlist
W      playlist-shuffle

f      cycle fullscreen ; show-text "Scale: ${window-scale}"
F      vf clr "" ; show-text "Filters cleared"

p      cycle-values video-aspect "16:9" "4:3" "2.35:1" "16:10"
P      ignore

g      ignore
G      ignore

j      ignore
J      ignore

l      cycle-values loop-file yes no ; show-text "${?=loop-file==yes:Looping enabled (file)}${?=loop-file==no:Looping disabled (file)}"
L      cycle-values loop-playlist yes no ; show-text "${?=loop-playlist==inf:Looping enabled}${?=loop-playlist==no:Looping disabled}"
CTRL+l ab-loop

u      ignore
U      ignore

y      osd-msg-bar seek +5
Y      osd-msg-bar seek +30
ALT+y  osd-msg-bar seek +120
CTRL+y osd-msg-bar seek +1 relative+exact

;      ignore
:      ignore

[      ignore
]      ignore

{      ignore
}      ignore

# [a] [r] [s] t [d] [h] [n] [e] [i] o '
#  A  [R] [S] T [D] [H] [N] [E]  I  O "

a      cycle audio                                                     # switch audio streams
A      ignore

r      cycle-values video-rotate 90 180 270 0
R      cycle-values video-rotate 270 180 90 0

s      cycle sub                                                       # cycle through subtitles
S      cycle sub-visibility
CTRL+s cycle secondary-sid

t      ignore
T      ignore

d      cycle-values window-scale "1.5" "2.0" "0.5" "1.0" ; show-text "Scale: ${window-scale}"
D      cycle edition
CTRL+d cycle video

h      osd-msg-bar seek -5
H      osd-msg-bar seek -30
ALT+h  osd-msg-bar seek -120
CTRL+h osd-msg-bar seek -1 relative+exact

n      add chapter +1
N      playlist-next ; show-text "${playlist-pos-1}/${playlist-count}"

e      add chapter -1
E      playlist-prev ; show-text "${playlist-pos-1}/${playlist-count}"

i      script-binding stats/display-stats
I      script-binding stats/display-stats-toggle
CTRL+i show-progress

o      ignore
O      ignore

'      ignore
"      ignore

# [z] x c v [b] [k] [m]  ,   .   /
# [Z] X C V [B] [K] [M] [<] [>]  ?

z      add video-zoom +0.05
Z      add video-zoom -0.05

x      screenshot window
X      screenshot each-frame

c      ignore
C      ignore

v      ignore
V      ignore

b      add speed +0.05
B      add speed -0.05
CTRL+b set speed 1.0

k      add sub-delay +0.10
K      add sub-delay -0.10
CTRL+k set sub-delay 0

m      add audio-delay +0.10
M      add audio-delay -0.10
CTRL+m set audio-delay 0

<      ignore
>      ignore

.      frame-step ; show-text "Frame: ${estimated-frame-number} / ${estimated-frame-count}"
,      frame-back-step ; show-text "Frame: ${estimated-frame-number} / ${estimated-frame-count}"

/      ignore
?      ignore

# [-] [=] [esc] [space] [BackSpace]
# [_] [+] [tab] [enter]

-      add sub-scale -0.05                  # decrease subtitle font size
_      add sub-scale +0.05                  # increase subtitle font size
=      add sub-pos -1                       # move subtitles up
+      add sub-pos +1                       # move subtitles down

ESC               cycle fullscreen
SPACE             cycle pause
IDEOGRAPHIC_SPACE cycle pause
TAB               cycle mute
ENTER             show-progress

BS                revert-seek
SHIFT+BS          set speed 1.0 ; set gamma 0 ; set brightness 0 ; set contrast 0 ; set saturation 0 ; set hue 0 ; show-text "Speed/Gamma/Brightness/Contrast/Saturation/Hue resetted"
ALT+BS            set video-pan-x 0 ; set video-pan-y 0 ; show-text "Pan resetted"
META+BS           set video-zoom 0 ; show-text "Zoom resetted"
